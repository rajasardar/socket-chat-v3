"use strict";

var path      = require("path");
var Sequelize = require("sequelize");
var config    = require(path.join(__dirname, '..', 'config', 'config.json'));
if(config.mysql.connection) {
	var sequelize = new Sequelize(config.mysql.connection, config.mysql.params);
} else {
	var sequelize = new Sequelize(config.mysql.database, config.mysql.username, config.mysql.password, config.mysql.params);
}

module.exports = sequelize;